package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import INF101.lab2.FridgeItem;


public class Fridge implements IFridge{

    int max_size = 20;
    int items = 0;
    List<FridgeItem> fridge = new ArrayList<FridgeItem>();


    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        items = fridge.size();
        return items;
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (items < totalSize()){
            fridge.add(item);
            items ++;
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (fridge.contains(item)){
            fridge.remove(item);
            items --;
        }
        else{
            throw new NoSuchElementException();
        }

    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        fridge.clear();
        items = 0;
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> expired = new ArrayList<FridgeItem>();
        for (int i = 0; i < nItemsInFridge(); i++){
            FridgeItem item = fridge.get(i);
            if(item.hasExpired()){
                fridge.remove(i);
                items --;
                i--;
                expired.add(item);
            }
        }

        return expired;
    }
    
}
